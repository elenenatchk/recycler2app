package com.example.recyclerviewpart2
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.example.recyclerviewpart2.databinding.FirstActivityBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: FirstActivityBinding
    private val user = mutableListOf<UserItemsModel>()
    private lateinit var adapter: UserAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = FirstActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initRecView()

    }

    private fun initRecView() {
        setInfo()
        adapter = UserAdapter(user, object : ItemListener {
            override fun onClick(position: Int) {
              }

        })
        binding.recyclerview.layoutManager = GridLayoutManager(this, 2)
        binding.recyclerview.adapter = adapter
    }
    private fun setInfo() {

        user.add(UserItemsModel("Elene ","Ntchk","kfjsdkf@gmail.com"))
        user.add(UserItemsModel("Elene ","Ntchk","kfjsdkf@gmail.com"))


    }
}