package com.example.recyclerviewpart2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewpart2.databinding.ItemsLayoutBinding

class UserAdapter(
    private val users: MutableList<UserItemsModel>,
    private val itemListener: ItemListener
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    inner class UserlViewHolder(private val binding: ItemsLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            val modelFirst = users[adapterPosition]
            binding.fname.text = modelFirst.fname
            binding.sname.text = modelFirst.sname
            binding.email.text = modelFirst.email


        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val ItemView=ItemsLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        val i= UserlViewHolder(ItemView)
        return i

    }

    override fun onBindViewHolder(holder: UserlViewHolder, position: Int) {
        holder.bind()

    }

    override fun getItemCount() = users.size
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

}

